/* Magic Mirror
 * Module: MMM-R6Siege
 *
 * By Willy Fritzsche https://willy-fritzsche.de
 */

Module.register('MMM-R6Siege', {

	// Default module config
	defaults: {
		playerData: {},
		uuids: [],
		platform: 'PC', // Possible: PC, PS4, XBOX
		updateInterval: 10800, // Seconds (3h),
		showInterval: 10, // Seconds
		fadeSpeed: 0, // Milliseconds
		cells: [
			{
				img: 'time.svg',
				first: data => `${Math.floor(data.stats.general.timePlayed / 60 / 60)}h`,
				second: data => `${Math.floor(data.stats.general.timePlayed / 60 % 60)}m`
			},
			{
				img: 'sword.svg',
				first: data => data.stats.general.played,
				second: data => data.stats.queue.ranked.played
			},
			{
				img: 'trophy.svg',
				first: data => (data.stats.general.won / data.stats.general.lost).toFixed(2),
				second: data => `${data.stats.general.won}/${data.stats.general.lost}`
			},
			{
				img: 'skull.svg',
				first: data => (data.stats.general.kills / data.stats.general.deaths).toFixed(2),
				second: data => `${data.stats.general.kills}/${data.stats.general.deaths}`
			}
		]
	},

	requiresVersion: '2.1.0',

	getStyles: function() {
		return ['MMM-R6Siege.css'];
	},

	start: function() {
		Log.info('Starting module: ' + this.name);

		this.status = 'loading';
		this.playerData = {};
		this.playerLoop = -1;

		this.sendSocketNotification('CONFIG', this.config);
		this.scheduleUpdate();
	},

	getDom: function() {
		const wrapper = document.createElement('div');

		if (this.status === 'loading') {
			wrapper.className = 'small dimmed';
			wrapper.innerText = 'Lade ...';

			return wrapper;
		}

		// Get data for next player
		const playerData = this.getNextPlayerData();

		wrapper.className = 'r6siege__container';

		const name = document.createElement('div');
		name.className = 'r6siege__name';
		name.innerText = playerData.name;

		const level = document.createElement('div');
		level.className = 'r6siege__level';
		level.innerText = 'Level ' + playerData.level;

		const stats = document.createElement('div');
		stats.className = 'r6siege__stats';

		this.config.cells.forEach(cellData => {
			const imgSrc = /^(http[s]?:\/\/|data:image\/)/i.test(cellData.img) ? cellData.img : `${this.data.path}icons/${cellData.img}`;

			const img = document.createElement('img');
			img.setAttribute('src', imgSrc);

			const first = document.createElement('div');
			first.innerText = cellData.first(playerData);

			const second = document.createElement('div');
			second.innerText = cellData.second(playerData);

			const cell = document.createElement('div');
			cell.appendChild(img);
			cell.appendChild(first);
			cell.appendChild(second);

			stats.appendChild(cell);
		});

		wrapper.appendChild(name);
		wrapper.appendChild(level);
		wrapper.appendChild(stats);

		return wrapper;
	},

	getNextPlayerData: function() {
		const uuids = Object.keys(this.config.playerData);

		this.playerLoop = this.playerLoop < uuids.length-1 ? this.playerLoop + 1 : 0;

		return this.config.playerData[uuids[this.playerLoop]];
	},

	scheduleUpdate: function() {
		if (typeof this.config.updateInterval !== 'number' || typeof this.config.showInterval !== 'number')
			return;
		
		setInterval(() => {
			if (this.status === 'active')
				this.sendSocketNotification('UPDATE_DATA', this.config);
		}, this.config.updateInterval * 1000);

		if (this.config.uuids.length > 0) {
			setInterval(() => {
				if (this.status === 'active')
					this.updateDom(this.config.fadeSpeed);
			}, this.config.showInterval * 1000);
		}
	},

	socketNotificationReceived: function(notification, payload) {
		if (notification === 'STARTED')
			this.status = 'active';
		else if (notification === 'PLAYER_DATA') {
			this.config.playerData = payload;
			this.updateDom();
		}
	}

});
