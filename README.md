# Module: MMM-R6Siege

The `MMM-R6Siege` module for the [MagicMirror](https://github.com/MichMich/MagicMirror) shows statistics from a list of Rainbow Six: Siege players. You can also customize the module to your needs.

![Preview](preview.gif)

## Installing the module

1. Navigate into your MagicMirror's `modules` folder.
2. Execute `git clone https://gitlab.com/willfri/MMM-R6Siege`
3. Go now into the `MMM-R6Siege` folder.
4. Execute `npm install`

## Using the module

To use this module, add it to the modules array in the `config/config.js` file:
```javascript
modules: [
	{
		module: "MMM-R6Siege",
		position: "top_left",	// This can be any of the regions.
		config: {
			// See 'Configuration options' for more information.
			uuids: [
				'abcd1234-abcd-1234-ef56-a1b2c3d4e5f6',
				'4321dcba-dcba-4321-65fe-6f5e4d3c2b1a'
			],
			email: 'your-email@example.com',
			password: 'verysecretpassword'
		}
	}
]
```

## Configuration options

The following properties can be configured:

| Option           | Description
| ------           | -----------
| `uuids`          | Array of UUIDs to be shown. To get the player UUID, visit [https://r6db.com](https://r6db.com) and search for the player. Copy the UUID from the URL and paste it into the config. <br><br> This value is **REQUIRED** <br> **Example:** `['abcd1234-abcd-1234-ef56-a1b2c3d4e5f6', '...']` <br> **Default value:** `[]`
| `email`          | Your email which you use to login to your Ubisoft account. <br><br> This value is **REQUIRED** <br> **Example:** `'your-email@example.com'` <br> **Default value:** `''`
| `password`       | Your password which you use to login to your Ubisoft account. <br><br> This value is **REQUIRED** <br> **Example:** `'verysecretpassword'` <br> **Default value:** `''`
| `platform`       | The platform to obtain player statistics. <br><br> This value is **OPTIONAL** <br> **Example:** `'PC'` <br> **Possible values:** `'PC'` or `'PS4'` or `'XBOX'` <br> **Default value:** `'PC'`
| `updateInterval` | The number of seconds to update the data. Do not choose too low to prevent blocking. <br><br> This value is **OPTIONAL** <br> **Example:** `21600` <br> **Possible values:** `int` <br> **Default value:** `10800` (3 hours)
| `showInterval`   | The number of seconds how long a player should be shown during rotation. <br><br> This value is **OPTIONAL** <br> **Example:** `30` <br> **Possible values:** `int` <br> **Default value:** `10`
| `fadeSpeed`      | The number of milliseconds for the transition between two players. If only one player is given, this value will be ignored. <br><br> This value is **OPTIONAL** <br> **Example:** `2000` <br> **Possible values:** `int` <br> **Default value:** `0`
| `cells`          | See below `Defining cells` for more information. <br><br> This value is **OPTIONAL** <br> **Example:** `[ { img: '...', first: '...', second: '...' }, { ... } ]` <br> **Possible values:** `array` <br> **Default value:** See below `Default value for cells`

## Defining cells

You can define your own cells if the default ones are not enough. It doesn't matter how many you create.
It is only important that a cell has the following format.

```javascript
{
	img: 'http://example.com/img/random.jpg',
	first: function(data) { return data.stats.general.played; },
	second: function(data) { return data.stats.queue.ranked.played; }
}
```

| Option   | Description
| ------   | -----------
| `img`    | The image to be displayed at the top of the cell. There are three ways to integrate an image. <br><br> **1:** Filename from the MagicMirror `modules/MMM-R6Siege/icons` folder. <br> **Example:** `'trophy.svg'` <br><br> **2:** URL <br> **Example:** `'http://example.com/img/random.jpg'` <br><br> **3:** Data-URL <br> **Example:** `'data:image/png;base64,iVBORw0KGgoAAAANS...'`
| `first`  | First text line of the cell. Required a function with one parameter and a return value which will be displayed. The parameter passes an object with values for name, level, statistics and ranked. For an example object see `example.json`. <br><br> **Example:** `function(data) { return data.stats.general.played; }`
| `second` | Second text line. Rest like `first`.

### Default value for `cells`

```javascript
[
	{
		img: 'time.svg',
		first: data => `${Math.floor(data.stats.general.timePlayed / 60 / 60)}h`,
		second: data => `${Math.floor(data.stats.general.timePlayed / 60 % 60)}m`
	},
	{
		img: 'sword.svg',
		first: data => data.stats.general.played,
		second: data => data.stats.queue.ranked.played
	},
	{
		img: 'trophy.svg',
		first: data => (data.stats.general.won / data.stats.general.lost).toFixed(2),
		second: data => `${data.stats.general.won}/${data.stats.general.lost}`
	},
	{
		img: 'skull.svg',
		first: data => (data.stats.general.kills / data.stats.general.deaths).toFixed(2),
		second: data => `${data.stats.general.kills}/${data.stats.general.deaths}`
	}
]
```

## Defining widths

Each cell has a default width of 80 pixels. And the entire module 320 pixels.

```css
.r6siege__container {
	min-width: 320px;
}

.r6siege__container .r6siege__stats > div {
	flex-basis: 80px;
}
```

This means that four modules fit per row. If the cells are larger and therefore no longer fit in one row, they are wrapped down. This can also be influenced by adjusting the module width. The CSS can be modified in the `css/custom.css` file.

## Libraries used

This module uses code from others, many thanks to the authors and contributors. Cheers

- [r6api](https://github.com/r6db/r6api) to fetch the data from Ubisoft