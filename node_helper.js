/* Magic Mirror
 * Module: MMM-R6Siege
 *
 * By Willy Fritzsche https://willy-fritzsche.de
 * 
 * Many thanks to the guy(s) of http://r6db.com for providing r6api (https://github.com/r6db/r6api)
 */

const NodeHelper = require('node_helper'),
      r6api = require('./lib/r6api');

module.exports = NodeHelper.create({

	start: function() {
		this.config = {};
	},

	socketNotificationReceived: function (notification, payload) {
		if (notification === 'CONFIG') {
			this.sendSocketNotification('STARTED');
			this.fetchData(payload);
		}
		else if (notification === 'UPDATE_DATA')
			this.fetchData(payload);
	},

	fetchData: function(config) {
		const uuids = config.uuids,
			  platform = config.platform,
			  playerData = {};
		
		if (Object.keys(uuids).length === 0
			|| !['PC', 'PS4', 'XBOX'].includes(platform)
			|| !config.email
			|| !config.password
			|| config.email === ''
			|| config.password === ''
		)
			return console.log('[MMM-R6Siege] Some required options are missing!');

		uuids.forEach(uuid => {
			playerData[uuid] = {
				uuid
			};
		});

		r6api.auth.setCredentials(config.email, config.password);

		Promise.all([
			r6api.api.getCurrentName(platform, uuids)
				.then(data => {
					data.map(player => {
						const uuid = player.id;
						
						playerData[uuid].uuid = uuid;
						playerData[uuid].name = player.name;
					})
				}),
			r6api.api.getStats(platform, uuids)
				.then(data => {
					data.map(player => {
						const uuid = player.id;
						delete player.id;

						playerData[uuid].stats = player;
					});
				}),
			r6api.api.getLevel(platform, uuids)
				.then(data => {
					data.map(player => {
						const uuid = player.id;

						playerData[uuid].level = player.level;
					});
				}),
			r6api.api.getRanks(platform, uuids)
				.then(data => {
					data.map(player => {
						const uuid = player.id;
						delete player.id;

						playerData[uuid].ranks = player;
					});
				})
        ])
        .then(() => this.sendSocketNotification('PLAYER_DATA', playerData))
		.catch(console.error);
	}
	
});
